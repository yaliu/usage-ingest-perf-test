package com.zuora.platform.stream.usage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.http.client.entity.GzipCompressingEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Yangming Liu(yaliu@zuora.com) 2023/03/24
 */
public class Main {
    static final String[] CHARGE_NAME_ARRAY = new String[] { "Onboarding Fee", "Storage", "Prepaid",
            "Base Subscription", "Recurring Discount" };
    static final String[] ACCOUNT_ARRAY = new String[] { "A00008001", "A00008058", "A00008036", "A00006510",
            "A00008053", "A00008043", "A00008046", "A00008038" };

//    static final String[] CHARGE_NAME_ARRAY = new String[] { "Volume Pricing" };
//    static final String[] ACCOUNT_ARRAY = new String[] { "2c92c0f8721c8caf0172297154686eaa" };

    static final String ENTITY_ID = System.getProperty("entity", "2c92c8fe-7a98-9ea5-017a-9c73069109cb");
    static final String TENANT_ID = System.getProperty("tenant", "10282");
//    static final String ENTITY_ID = System.getProperty("entity", "11e643f4-a3f8-a9da-b061-0025904c57d6");
//    static final String TENANT_ID = System.getProperty("tenant", "12552");
    // static final String ENTITY_ID = System.getProperty("entity", "1");
    // static final String TENANT_ID = System.getProperty("tenant", "1");

    static final String FORMAT = "{ADDRESS}/usage/bulk";
    static final Random RANDOM = new Random();

    public static void main(String... args) throws Exception {
        String threads = System.getProperty("threads", "100");
        String address = System.getProperty("address", "http://rtpp-service-stg01.stg.auw2.zuora");
        String batchSize = System.getProperty("batchSize", "256");
        String execTimes = System.getProperty("execTimes"); // , "10000"
        String duration = System.getProperty("duration"); // , "120"
        String token = System.getProperty("token");
        String compress = System.getProperty("compress", "false");

        if (StringUtils.isNotBlank(execTimes) && StringUtils.isNotBlank(duration)) {
            throw new IllegalStateException("execTimes and duration cannot be configured together.");
        } else if (StringUtils.isBlank(execTimes) && StringUtils.isBlank(duration)) {
            throw new IllegalStateException("execTimes and duration cannot both be empty.");
        }
        Config.getInstance().setCompress(Boolean.parseBoolean(compress));
        Config.getInstance().setThreads(Integer.parseInt(threads));
        Config.getInstance().setAddress(FORMAT.replace("{ADDRESS}", address));
        Config.getInstance().setBatchSize(Integer.parseInt(batchSize));
        Config.getInstance().setToken(token);
        if (StringUtils.isNotBlank(duration)) {
            Config.getInstance().setDuration(Integer.parseInt(duration));
        } else {
            Config.getInstance().setExecTimes(Integer.parseInt(execTimes));
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        // calendar.set(Calendar.YEAR, 2023);
        // calendar.set(Calendar.MONTH, 3);
        // calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long endMillis = calendar.getTimeInMillis();
        Config.getInstance().setStartTime(endMillis - 1000L * 60 * 60 * 24 * 30);
        Config.getInstance().setEndTime(endMillis);

        if (Config.getInstance().getDuration() > 0) {
            System.out.printf("--- threads: %s, batch: %s, duration: %s(s), address: %s, compress: %s ---%n" //
                    , threads, batchSize, duration, address, compress);
        } else {
            System.out.printf("--- threads: %s, batch: %s, exec-times: %s, address: %s, compress: %s ---%n" //
                    , threads, batchSize, execTimes, address, compress);
        }

        startup();
    }

    static final Metrics TOTAL = new Metrics();

    public static void startup() {
        int threads = Config.getInstance().getThreads();
        final ExecutorService service = Executors.newFixedThreadPool(threads);
        Runtime.getRuntime().addShutdownHook(new Thread("mutator-server-shutdown-hook") {
            @Override
            public void run() {
                service.shutdownNow();
            }
        });

        Future<?>[] futures = new Future<?>[threads];
        long startMillis = System.currentTimeMillis();
        for (int i = 0; i < threads; i++) {
            futures[i] = service.submit(new WorkImpl(i, startMillis));
        }

        long interval = 5 * 1000L;
        long nextMillis = System.currentTimeMillis() + interval;
        long lastTotal = 0, lastError = 0, lastDuration = 0;
        do {
            int count = 0;
            for (int i = 0; i < threads; i++) {
                Future<?> future = futures[i];
                boolean done = future.isDone();
                count += done ? 1 : 0;
            }

            if (count == threads) {
                break;
            }
            long millis = nextMillis - System.currentTimeMillis();
            if (millis > 0) {
                waitingFor(millis);
                continue;
            }

            Tuple5 tuple = TOTAL.summary();
            long currentTotal = tuple.total, currentError = tuple.error, currentDuration = tuple.duration;
            long previouTotal = lastTotal, previouError = lastError, previouDuration = lastDuration;

//            long currentTotal = TOTAL.total(), currentError = TOTAL.error(), currentDuration = TOTAL.duration;
//            long previouTotal = lastTotal, previouError = lastError, previouDuration = lastDuration;

            long delta = currentTotal - previouTotal;
            long tps = (1000L * delta) / interval;
            System.out.printf(
                    "-- requests:%6d(%5d), tps:%5d, latency:%4d // avg=%6.1f, std=%6.1f, p75=%5.0f, p90=%5.0f, p99=%5.0f%n" //
                    , delta, currentError - previouError, tps //
                    , (currentDuration - previouDuration) / (delta == 0 ? 1 : delta) //
                    , tuple.mean, tuple.stdv, tuple.p75, tuple.p90, tuple.p99);

            nextMillis += interval;
            lastTotal = currentTotal;
            lastError = currentError;
            lastDuration = currentDuration;
        } while (true);

        long cost = (System.currentTimeMillis() - startMillis) / 1000L;
        long tps = (TOTAL.total() * 1000L) / (System.currentTimeMillis() - startMillis);
        System.out.printf("summary: total= %d(%s), records= %s, cost= %d(s), tps= %d%n" //
                , TOTAL.total(), TOTAL.error, TOTAL.total * Config.getInstance().getBatchSize(), cost, tps);

        service.shutdownNow();
    }

    public static void waitingFor(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public static class WorkImpl implements Runnable {
        static final ObjectMapper MAPPER = //
                new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        private final int index;
        private volatile int sequence = 1;
        private PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        private CloseableHttpClient httpClient;

        private volatile long beginMillis;

        private volatile int total;
        private volatile int records;

        public WorkImpl(int index, long beginMillis) {
            this.index = index;
            this.connManager.setMaxTotal(50);
            this.connManager.setDefaultMaxPerRoute(50);
            this.httpClient = HttpClientBuilder.create().setConnectionManager(connManager).build();
            this.beginMillis = beginMillis;
        }

        public void run() {
            if (Config.getInstance().getDuration() > 0) {
                executeByDuration();
            } else {
                executeByTimes();
            }
        }

        private void executeByDuration() {
            int duration = Config.getInstance().getDuration();
            long startMillis = System.currentTimeMillis();
            long shutdownMillis = beginMillis + duration * 1000L;
            while (startMillis < shutdownMillis) {
                try {
                    this.execute();
                    long endMillis = System.currentTimeMillis();
                    TOTAL.duration(endMillis - startMillis, true);
                    startMillis = endMillis;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    long endMillis = System.currentTimeMillis();
                    TOTAL.duration(endMillis - startMillis, false);
                    startMillis = endMillis;
                } finally {
                    this.total++;
                }
            }
        }

        private void executeByTimes() {
            int execTimes = Config.getInstance().getExecTimes();
            while (this.total < execTimes) {
                long startMillis = System.currentTimeMillis();
                try {
                    this.execute();
                    TOTAL.duration(System.currentTimeMillis() - startMillis, true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    TOTAL.duration(System.currentTimeMillis() - startMillis, false);
                } finally {
                    this.total++;
                }
            }
        }

        public void execute() throws IOException {
            HttpPost request = new HttpPost(Config.getInstance().getAddress());

            String token = Config.getInstance().getToken();
            if (StringUtils.isBlank(token)) {
                request.addHeader("Zuora-Tenant-Id", TENANT_ID);
                request.addHeader("Zuora-Entity-Ids", ENTITY_ID);
            } else {
                request.addHeader("Authorization", "Bearer ".concat(token));
            }

            request.addHeader("Content-Type", "application/json");

            int batchSize = Config.getInstance().getBatchSize();
            List<Object> values = new ArrayList<>();
            for (int i = 0; i < batchSize; i++) {
                Object data = this.generate();
                values.add(data);
            }

            String content = MAPPER.writeValueAsString(values);
            if (Config.getInstance().isCompress()) {
                request.setEntity(new GzipCompressingEntity(new StringEntity(content)));
            } else {
                request.setEntity(new StringEntity(content));
            }

            CloseableHttpResponse response = //
                    this.httpClient.execute(request);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copy(response.getEntity().getContent(), baos);

            int statusCode = response.getStatusLine().getStatusCode();

            response.close();

            if (statusCode >= 400) {
                String reason = response.getStatusLine().getReasonPhrase();
                throw new IllegalStateException(String.format("error: code= %s, reason= %s", statusCode, reason));
            }
        }

        static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        public Object generate() {
            long startTime = Config.getInstance().getStartTime();
            long endTime = Config.getInstance().getEndTime();
            long interval = endTime - startTime;
            long timestamp = startTime + RANDOM
                    .nextInt((int) (interval > Integer.MAX_VALUE || interval < 0 ? Integer.MAX_VALUE : interval));
            double amount = Math.abs(1_000 * RANDOM.nextDouble());
            double quantity = Math.abs(1_000_000 * RANDOM.nextDouble());

            if ("10282".equalsIgnoreCase(TENANT_ID)) {
                Data10282 data = new Data10282();
                // data.CustomerId = ACCOUNT_ARRAY[RANDOM.nextInt(ACCOUNT_ARRAY.length)];
                data.CustomerId = ACCOUNT_ARRAY[this.records++ % ACCOUNT_ARRAY.length];
                data.UsageIdentifier = CHARGE_NAME_ARRAY[RANDOM.nextInt(CHARGE_NAME_ARRAY.length)];
                data.UsageDate = sdf.format(new Date(timestamp));
                data.Amount = amount;
                data.Quantity = quantity;
                return data;
            } else if ("9".equalsIgnoreCase(TENANT_ID)) {
                String changeName = CHARGE_NAME_ARRAY[RANDOM.nextInt(CHARGE_NAME_ARRAY.length)];

                Data9 data = new Data9();
                data.subscription_id = changeName;
                data.charge_id = changeName;
//                 data.account_id = ACCOUNT_ARRAY[RANDOM.nextInt(ACCOUNT_ARRAY.length)];
                data.account_id = ACCOUNT_ARRAY[this.records++ % ACCOUNT_ARRAY.length];
                data.pv01 = quantity;
                data.uom = changeName;
//                data.trade_date = timestamp;
                data.trade_date = sdf.format(new Date(timestamp));
                return data;
            } else {
                String changeName = CHARGE_NAME_ARRAY[RANDOM.nextInt(CHARGE_NAME_ARRAY.length)];

                Data1 data = new Data1();
                data.eventId = String.format("%2s-%s", this.index, this.sequence++);
                data.subscriptionId = changeName;
                data.chargeId = changeName;
                // data.accountId = ACCOUNT_ARRAY[RANDOM.nextInt(ACCOUNT_ARRAY.length)];
                data.accountId = ACCOUNT_ARRAY[this.records++ % ACCOUNT_ARRAY.length];
                data.quantity = quantity;
                data.uom = changeName;
                data.eventTime = timestamp;
                return data;
            }
        }

        public int getIndex() {
            return index;
        }
    }

    static class Tuple5 {
        public long total;
        public long error;
        public long duration;

        public double mean;
        public double stdv;
        public double p75;
        public double p90;
        public double p99;
    }

    static class Metrics {
        public volatile long total = 0;
        public volatile long error = 0;
        public long duration = 0;
        public DescriptiveStatistics statistics = new DescriptiveStatistics();

        public synchronized void duration(long duration, boolean success) {
            this.duration += duration;
            this.total++;
            this.error += success ? 0 : 1;
            statistics.addValue(duration);
        }

        public synchronized Tuple5 summary() {
            Tuple5 tuple = new Tuple5();
            tuple.total = total;
            tuple.error = error;
            tuple.duration = duration;

            tuple.mean = statistics.getMean();
            tuple.stdv = statistics.getStandardDeviation();
            tuple.p75 = statistics.getPercentile(75);
            tuple.p90 = statistics.getPercentile(90);
            tuple.p99 = statistics.getPercentile(99);

            statistics.clear();

            return tuple;
        }

        public synchronized long total() {
            return total;
        }

        public synchronized long error() {
            return error;
        }
    }

    static class Data1 {
        public String transactionId;
        public String eventId;
        public String subscriptionId;
        public String chargeId;
        public String accountId;
        public double quantity;
        public String uom;
        public long eventTime;
    }

    static class Data9 {
        public String subscription_id;
        public String charge_id;
        public String account_id;
        public double pv01;
        public String uom;
        public String trade_date;
    }

    static class Data10282 {
        public String UsageIdentifier;
        public String CustomerId;
        public String UsageDate;
        public double Amount;
        public double Quantity;
    }
}
